## 功能介绍 
    
少年宫小程序包括资讯动态，机构简介，师资展示，缤纷社团，活动演出，家长频道，教学研究，课程培训，活动预约，活动日历，我的今日预约，后台内容管理，后台预约管理，后台预约核销等功能。通过数字化小程序，打造网上少年宫，方便辖区青少年第一时间了解到街道或者自己周边社区举办的各项青少年活动。

 ![输入图片说明](demo/%E4%BA%8C%E7%BB%B4%E7%A0%81.png)

## 技术运用
- 本项目使用微信小程序平台进行开发。
- 使用腾讯专门的小程序云开发技术，云资源包含云函数，数据库，带宽，存储空间，定时器等，资源配额价格低廉，无需域名和服务器即可搭建。
- 小程序本身的即用即走，适合小工具的使用场景，也适合快速开发迭代。
- 云开发技术采用腾讯内部链路，没有被黑客攻击的风险，安全性高且免维护。
- 资源承载力可根据业务发展需要随时弹性扩展。  



## 作者
- 如有疑问，欢迎骚扰联系我鸭：开发交流，技术分享，问题答疑，功能建议收集，版本更新通知，安装部署协助，小程序开发定制等。
- 俺的微信:
 
 
![输入图片说明](demo/author-base.png)


## 演示 

  ![输入图片说明](demo/%E4%BA%8C%E7%BB%B4%E7%A0%81.png)
 

## 安装

- 安装手册见源码包里的word文档




## 截图

![输入图片说明](demo/1%E9%A6%96%E9%A1%B5.png)
![输入图片说明](demo/2%E8%B5%84%E8%AE%AF.png)
![输入图片说明](demo/3%E6%9C%BA%E6%9E%84%E7%AE%80%E4%BB%8B.png)
 ![输入图片说明](demo/4%E9%A2%84%E7%BA%A6%E6%97%A5%E5%8E%86.png)
![输入图片说明](demo/5%E6%88%91%E7%9A%84.png)
![输入图片说明](demo/6%E7%A4%BE%E5%9B%A2.png)
![输入图片说明](demo/7%E6%B4%BB%E5%8A%A8%E6%BC%94%E5%87%BA.png)
![输入图片说明](demo/8%E5%AE%B6%E9%95%BF%E9%A1%BB%E7%9F%A5.png)
![输入图片说明](demo/9%E6%95%99%E5%AD%A6%E7%A0%94%E7%A9%B6.png)
![输入图片说明](demo/10%E8%AF%BE%E7%A8%8B%E5%9F%B9%E8%AE%AD.png)
![输入图片说明](demo/11%E6%B4%BB%E5%8A%A8%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/12%E9%A9%AC%E4%B8%8A%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/13%E9%A2%84%E7%BA%A6%E6%88%90%E5%8A%9F.png)

## 后台管理系统截图
 ![输入图片说明](demo/14%E5%90%8E%E5%8F%B0-%E9%A6%96%E9%A1%B5.png)
![输入图片说明](demo/15%E5%90%8E%E5%8F%B0-%E5%86%85%E5%AE%B9%E6%A8%A1%E5%9D%97.png)

![输入图片说明](demo/16%E5%90%8E%E5%8F%B0-%E6%96%87%E7%AB%A0%E6%B7%BB%E5%8A%A0.png)
![输入图片说明](demo/17%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E7%AE%A1%E7%90%86.png)
![输入图片说明](demo/18%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E8%8F%9C%E5%8D%95.png)
![输入图片说明](demo/19%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E5%90%8D%E5%8D%95%E5%AF%BC%E5%87%BA.png)
![输入图片说明](demo/20%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E5%90%8D%E5%8D%95.png)
![输入图片说明](demo/21%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E5%90%8D%E5%8D%95%E7%AE%A1%E7%90%86.png)
![输入图片说明](demo/22%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E6%B7%BB%E5%8A%A0.png)
![输入图片说明](demo/23%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E6%A0%B8%E9%94%80.png)
